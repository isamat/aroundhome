class Partner < ApplicationRecord
  extend Enumerize
  acts_as_mappable

  validates_presence_of :address
  validates :radius, numericality: { greater_than: 0 }

  enumerize :materials, in: [:wood, :carpet, :tiles], multiple: true

  scope :within, -> (point) {
    where("ST_Distance(address, 'POINT(? ?)') <= radius", point.lon, point.lat)
  }

  scope :experienced_in, ->(material) {
    where(":material = ANY (materials)", material: material)
  }

  def self.find_matches(customer)
    self.within(customer.address).experienced_in(customer.material)
  end
end