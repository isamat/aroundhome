class Api::V1::PartnersController < Api::V1::ApiController
  before_action :set_partner, only: :show
  before_action :set_customer, only: :index
  def index
    @partners = Partner.order(rating: :desc).find_matches(@customer)
    render json: { partners: @partners }
  end

  def show
    render json: @partner
  end

  private

  def set_partner
    @partner = Partner.find(params[:id])
  end

  def set_customer
    @customer = Customer.find(params[:customer_id])
  end
end