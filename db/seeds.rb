Customer.create(address: "POINT(#{52.5200} #{13.4050})", phone: "+49 1764561233", area: 86, material: 'wood')
Customer.create(address: "POINT(#{52.5200} #{13.4054})", phone: "+49 17645612307", area: 86, material: 'tiles')


Partner.create(address: "POINT(#{52.4983} #{13.4066})", radius: 4500, rating: 8.8, materials: ['wood'])
Partner.create(address: "POINT(#{52.4983} #{13.4066})", radius: 2500, rating: 7.6, materials: ['wood tiles'])
Partner.create(address: "POINT(#{52.4983} #{13.4066})", radius: 2500, rating: 6.6, materials: ['wood tiles'])
Partner.create(address: "POINT(#{52.4983} #{13.4066})", radius: 3500, rating: 6.5, materials: ['wood tiles'])