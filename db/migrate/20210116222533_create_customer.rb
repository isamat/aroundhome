class CreateCustomer < ActiveRecord::Migration[6.0]
  def change
    create_table :customers do |t|
      t.string :material
      t.st_point :address, geographic: true
      t.float :area
      t.string :phone

      t.timestamps
    end
      add_index :customers, :address, using: :gist
  end
end
