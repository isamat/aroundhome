class CreatePartner < ActiveRecord::Migration[6.0]
  def change
    create_table :partners do |t|
      t.string :materials, array: true
      t.st_point :address, geographic: true
      t.float :radius
      t.float :rating

      t.timestamps
    end
    add_index :partners, :address, using: :gist
  end
end
