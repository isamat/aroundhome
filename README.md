# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version 2.6.1

* Rails version 6.0.3.4

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...

You will need to install Postgis and postgres sql.

There are two endpoints currently one to fetch list of partners by providing customer id
example : localhost:3000/api/v1/partners?customer_id

The second is to view partner info localhost:3000/api/v1/partners/id