Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      resources :partners, only: [:index, :show]
    end
  end
end
